package com.example.generatordemo;

import cn.hutool.cron.CronUtil;
import com.example.generatordemo.commons.schedule.TestTask;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.generatordemo.mapper")
public class GeneratorDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeneratorDemoApplication.class, args);

//        CronUtil.schedule("*/5 * * * * *", new TestTask());
//
//        //支持秒级
//        CronUtil.setMatchSecond(true);
//        //开启定时任务
//        CronUtil.start(true);
    }

}
