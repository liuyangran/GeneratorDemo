package com.example.generatordemo.service;

import com.example.generatordemo.model.entity.UserDo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuyangran
 * @since 2022-03-07
 */
public interface UserService extends IService<UserDo> {

}
