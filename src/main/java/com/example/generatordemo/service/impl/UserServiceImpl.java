package com.example.generatordemo.service.impl;

import com.example.generatordemo.model.entity.UserDo;
import com.example.generatordemo.mapper.UserMapper;
import com.example.generatordemo.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuyangran
 * @since 2022-03-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserDo> implements UserService {

}
