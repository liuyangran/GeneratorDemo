package com.example.generatordemo.mapper;

import com.example.generatordemo.model.entity.UserDo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuyangran
 * @since 2022-03-07
 */
public interface UserMapper extends BaseMapper<UserDo> {

    void updateBatch (@Param("userDos") List<UserDo> userDos);
}
