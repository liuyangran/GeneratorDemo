package com.example.generatordemo.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuyangran
 * @since 2022-03-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user")
public class UserDo extends Model<UserDo> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    @TableField(value = "name")
    @NotEmpty(message = "姓名不能为空")
    private String name;

    /**
     * 年龄
     */
    @TableField("age")
    @Range(max = 100, min = 10, message = "年龄需要在10到100之间")
    private Integer age;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 创建日期
     */
    @TableField(value = "createDate", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createDate;

    /**
     * 版本，用于乐观锁
     */
    @Version
    @TableField(exist = false)
    private int version;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
