package com.example.generatordemo.model;


import com.example.generatordemo.commons.constants.SysConstant;

/**
 * 返回结果的枚举信息
 */
public enum ResultEnum {
    //201为默认异常
    //用户相关
    USER_HAVE_NO_PERMISSION(210, "用户没有该权限"),
    USER_IS_LOCKED(211, "账号被锁"),
    USER_ERROR_USERNAME_OR_PASSWORD(212, "用户名或者密码错误"),
    USER_IS_INVALID(213, "用户没有登录或者已失效,请重新登录"),
    USER_IS_EXISTED(213, "该用户名已经存在"),
    USER_IS_NOT_EXISTED(214, "用户不存在"),
    TOKEN_IS_EXPIRED(215, "用户过期,请重新登录"),
    USER_DATA_ERROR(216, "用户数据错误"),
    //缺陷相关
    DEFECT_IS_CLAIMED(221, "该缺陷已经被认领"),
    DEFECT_IS_STARTED(222, "该缺陷已经开始执行"),
    DEFECT_IS_FEEDBACK(223, "该缺陷已经被反馈"),
    DEFECT_IS_WORK_HOURS_CONFIRMED(224, "该缺陷已经工时确认"),
    DEFECT_IS_DUTY_CONFIRMED(225, "该缺陷已经被值班确认"),
    DEFECT_IS_DELAYED(226, "该缺陷已经被延期"),
    DEFECT_EXECUTE_NUMBER_IS_MAXIMUM(227, "缺陷执行数达到上限,请先完成消缺中的缺陷"),
    //菜单相关
    FEATURE_IS_EXISTED(241, "该菜单名或菜单code编码已经存在"),
    FEATURE_TYPE_IS_ILLEGAL(242, "菜单类型是非法的"),
    // 公司相关
    SUBJECT_IS_EXISTED(251, "公司名或者公司code已存在"),
    //部门相关
    DEPARTMENT_CODE_IS_NULL(261, "当前部门编码未空,请先设置编码"),
    DEPARTMENT_IS_EXISTED(262, "部门名称或者部门code已经存在"),
    DEPARTMENT_TYPE_CAN_NOT_HAVE_CHILDREN(263, "该部门类型不允许添加子部门"),
    DEPARTMENT_TYPE_IS_ILLEGAL(264, "部门类型是非法的"),
    //文件相关
    FILE_IS_BIG(271, "文件不允许超过" + SysConstant.FILE_SIZE + "MB"),
    FILE_TYPE_IS_NOT_ALLOWED(272, "只能上传" + SysConstant.FILE_TYPE + "类型的文件"),
    //岗位相关
    //知识库
    KNOWLEDGE_IS_CHECKED(290, "你已经审核了该记录"),
    //考试相关
    EXAM_COUNT_IS_LIMIT(300, "考试次数已达到上限"),
    EXAM_IS_UNFINISHED(301, "你有未完成的考试"),
    EXAM_PAPER_HAVE_UNFINISHED_QUESTION(302, "还有题目未作答,请填完再交卷"),
    EXAM_IS_TIMEOUT(303, "该考试已经超时,无法继续考试,已交卷"),
    //维护相关
    MAINTENANCE_TEMPLATE_HAVE_RECORD(310, "该维护配置已经有检修记录,无法删除"),
    //运行日志
    DAILY_INSPECTION_HAVE_NO_SUCCESSOR(320, "当前记录无接班人,请先接班"),
    //检修日志
    DAILY_MAINTENANCE_HAVE_NO_SUCCESSOR(320, "当前记录无接班人,请先接班"),
    //绩效管理
    PERFORMANCE_SCORE_IS_BIGGER_THAN_WEIGHT(330, "得分不能比权重分大"),
    ROLE_CODE_IS_EXISTED(340, "角色code已经存在"),
    // 请假相关
    LEAVE_NO_PERMISSION_APPLY(350, "用户不能申请当前类型的假期"),
    LEAVE_UNKNOW_TYPE(351, "未知的假期类型"),
    LEAVE_BEYOND_QUOTA(352, "超出剩余假期额度"),
    LEAVE_INVALID_DURATION(353, "无效的请假时长"),
    LEAVE_NO_PERMISSION_APPLY_FOR_OTHER(354, "无权为此员工申请假期"),
    LEAVE_NOT_FOUND(355, "未找到对应的请假数据"),
    // 审批相关
    APPROVAL_WRONG_TYPE(370, "错误的审批类型"),
    APPROVAL_HAVE_DONE(371, "无法审批已经审批完成的数据"),
    APPROVAL_REPEAT(372, "无效的重复审批"),
    //其他
    PARENT_HAVE_CHILDREN(490, "该数据含有子节点,无法删除"),
    SOURCE_MONTH_HAVE_NO_RECORD(491, "复制月没有数据"),
    TARGET_MONTH_HAVE_RECORD(492, "目标月已经存在数据"),

    EMPTY_PARAM(493, "参数为空"),
    EMPTY_CLASS(494, "执行类不存在"),
    CRON_IS_ILLEGAL(495, "cron表达式非法"),
    WRONG_PARAM_TYPE(496, "错误的参数类型"),
    ERROE_WHEN_SAVE_DATA(497, "数据存储失败");


    private Integer code;
    private String msg;

    ResultEnum() {
    }

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
