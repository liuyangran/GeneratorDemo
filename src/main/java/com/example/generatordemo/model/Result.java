package com.example.generatordemo.model;

import com.example.generatordemo.commons.constants.SysConstant;

public class Result {
    private int code;
    private String msg;
    /**
     * 数据
     */
    private Object data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    private static Result build(int code, Object data, String msg) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }

    public static Result ok(Object data) {
        return build(SysConstant.RESPONSE_OK_CODE, data, SysConstant.RESPONSE_OK_MSG);
    }


    public static Result ok() {
        return build(SysConstant.RESPONSE_OK_CODE, null, SysConstant.RESPONSE_OK_MSG);
    }

    public static Result fail(String msg) {
        return build(SysConstant.RESPONSE_FAIL_CODE, null, msg);
    }


    public static Result fail(Integer code, String msg) {
        return build(code, null, msg);
    }

    public static Result fail(ResultEnum resultEnum) {
        return build(resultEnum.getCode(), null, resultEnum.getMsg());
    }


    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

}
