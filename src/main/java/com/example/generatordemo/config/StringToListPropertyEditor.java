package com.example.generatordemo.config;

import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;

public class StringToListPropertyEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String[] resultArr = null;
        if (StringUtils.hasText(text)) {
            resultArr = text.split("_");
        }
        setValue(resultArr);
    }
}
