package com.example.generatordemo.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("---------插入时自动填充-----------");
        this.fillStrategy(metaObject,"createDate", LocalDateTime.now());
//        this.strictInsertFill(metaObject, "createDate", LocalDateTime::now, LocalDateTime.class);
//        this.strictInsertFill(metaObject, "createDate", LocalDateTime.class, LocalDateTime.now());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("---------修改时自动填充-----------");
        this.fillStrategy(metaObject,"createDate", LocalDateTime.now());
//        this.strictUpdateFill(metaObject, "createDate", LocalDateTime::now, LocalDateTime.class);
//        this.strictUpdateFill(metaObject, "createDate", Date.class, new Date());
    }
}
