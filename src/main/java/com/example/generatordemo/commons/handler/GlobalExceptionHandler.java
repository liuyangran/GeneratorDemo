package com.example.generatordemo.commons.handler;

import com.example.generatordemo.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理bean对象validation异常 (@Validated)
     * @param ex ConstraintViolationException对象
     * @return Result
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public Result resolveConstraintViolationException(ConstraintViolationException ex){

        // 获取验证异常信息集合
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        // 获取验证信息
        if(CollectionUtils.isNotEmpty(constraintViolations)){
            StringBuilder msgBuilder = new StringBuilder();
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                msgBuilder.append(constraintViolation.getMessage()).append(",");
            }
            String errorMessage = msgBuilder.toString();
            // 去除最后一个逗号
            if(errorMessage.length()>1){
                errorMessage = errorMessage.substring(0,errorMessage.length()-1);
            }
            return Result.fail(errorMessage);
        }
        ex.printStackTrace();
        return Result.fail(ex.getMessage());
    }

    /**
     * 处理方法validation异常 (@Valid)
     * @param ex MethodArgumentNotValidException对象
     * @return Result
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Result resolveMethodArgumentNotValidException(MethodArgumentNotValidException ex){

        List<ObjectError> objectErrors = ex.getBindingResult().getAllErrors();
        if (CollectionUtils.isNotEmpty(objectErrors)) {
            StringBuilder msgBuilder = new StringBuilder();
            for (ObjectError objectError : objectErrors) {
                msgBuilder.append(objectError.getDefaultMessage()).append(",");
            }
            String errorMessage  = msgBuilder.toString();
            if (errorMessage.length() > 0) {
                errorMessage = errorMessage.substring(0, errorMessage.length() - 1);
            }
            return Result.fail(errorMessage);
        }
        ex.printStackTrace();
        return Result.fail(ex.getMessage());
    }
}
