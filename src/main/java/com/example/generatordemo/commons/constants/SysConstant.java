package com.example.generatordemo.commons.constants;


public class SysConstant {

    /**
     * 默认分隔符
     */
    public static final String DEFAULT_SPLIT_SEPARATOR = ",";
    /**
     * 点分隔符
     */
    public static final String POINT_SPLIT_SEPARATOR = ".";
    /**
     * 横线分隔符
     */
    public static final String LINE_SPLIT_SEPARATOR = "-";

    /**
     * 多线程设置为10个
     */
    public static final Integer THREAD_COUNT = 10;

    /**
     * 默认的parentId值
     */
    public static final Integer PARENT_ID = 0;
    /**
     * 默认parentName值
     */
    public static final String PARENT_NAME = "无";


    /**
     * 数据库批量操作的上限为1000;
     */
    public static final Integer BATCH_SIZE = 1000;
    /**
     * 删除或覆盖操作的确认值,默认为1
     */
    public static final Integer CONFIRM_TYPE_VALUE = 1;
    /**
     * 本地文件前缀
     */
    public static final String LOCAL_FILE_PREFIX = "file://";
    /**
     * 文件存储路径
     */
    public static final String FILE_PATH = "/data/howell_file/file/";
    /**
     * 文件上传大小,单位MB
     */
    public static final Integer FILE_SIZE = 20;
    /**
     * 文件类型
     */
    public static final String FILE_TYPE = "txt,png,jpeg,jpg,xls,xlsx,doc,docx,pdf";
    /**
     * 接口请求成功返回的code值
     */
    public static final Integer RESPONSE_OK_CODE = 200;
    /**
     * 结构请求成功返回的msg值
     */
    public static final String RESPONSE_OK_MSG = "操作成功";
    /**
     * 接口请求失败返回的code值
     */
    public static final Integer RESPONSE_FAIL_CODE = 201;
    /**
     * 结构请求失败的返回的msg值
     */
    public static final String RESPONSE_FAIL_MSG = "操作失败";
    /**
     * 日期时间格式
     */
    public static final String DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * 日期格式
     */
    public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    /**
     * 时间格式
     */
    public static final String TIME_FORMAT_PATTERN = "HH:mm:ss";
    /**
     * 年月格式
     */
    public static final String MONTH_FORMAT_PATTERN = "yyyy-MM";
    /**
     * 公司部门树形结构的公司key前缀
     */
    public static final String SUBJECT_KEY_PREFIX = "0-";
    /**
     * 公司部门树形结构的部门key前缀
     */
    public static final String DEPARTMENT_KEY_PREFIX = "1-";
    /**
     * 可访问的公司部门的数据缓存的key前缀
     */
    public static final String SUBDEPT_KEY_PREFIX = "shiro:subdept:";
    /**
     * 可访问的公司部门的数据缓存的key公司后缀
     */
    public static final String SUBDEPT_KEY_SUB_SUFFIX = ":sub";
    /**
     * 可访问的公司部门的数据缓存的key部门后缀
     */
    public static final String SUBDEPT_KEY_DEPT_SUFFIX = ":dept";

}
