package com.example.generatordemo.commons.annotation;

import com.example.generatordemo.commons.annotation.constraint.HandsomeBoyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {HandsomeBoyValidator.class})
public @interface HandsomeBoy {

    String message() default "zzz最帅";
    String name();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
