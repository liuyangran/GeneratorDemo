package com.example.generatordemo.commons.annotation.constraint;

import com.example.generatordemo.commons.annotation.HandsomeBoy;
import com.example.generatordemo.model.entity.UserDo;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HandsomeBoyValidator implements ConstraintValidator<HandsomeBoy, UserDo> {

    private String name;

    @Override
    public void initialize(HandsomeBoy constraintAnnotation) {
        name = constraintAnnotation.name();
    }

    @Override
    public boolean isValid(UserDo userDo, ConstraintValidatorContext constraintValidatorContext) {
        return name ==null || name.equals(userDo.getName());
    }
}
