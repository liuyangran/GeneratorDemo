package com.example.generatordemo.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.example.generatordemo.commons.annotation.HandsomeBoy;
import com.example.generatordemo.model.Result;
import com.example.generatordemo.model.entity.UserDo;
import com.example.generatordemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuyangran
 * @since 2022-03-07
 */
@RestController
@RequestMapping("/user/")
@Validated
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test")
    public Result testValidate(@Valid @RequestBody UserDo user, BindingResult bindingResult) {

        if(bindingResult.hasErrors()){
            StringBuilder msgBuilder = new StringBuilder();
            for (ObjectError allError : bindingResult.getAllErrors()) {
                msgBuilder.append(allError.getDefaultMessage()).append(",");
            }
            String errorMessage  = msgBuilder.toString();
            if (errorMessage.length() > 0) {
                errorMessage = errorMessage.substring(0, errorMessage.length() - 1);
            }
            System.out.println(errorMessage);
            return Result.fail(errorMessage);
        }
        return Result.ok();
    }

    @GetMapping("/list")
    public Result testSelect(@Max(value = 10, message = "最大值为{mybatis-plus.mapper-locations}") Integer id,
                             @HandsomeBoy(name = "test", message = "验证不通过") String name){

        return Result.ok(userService.getById(id));
    }
}
