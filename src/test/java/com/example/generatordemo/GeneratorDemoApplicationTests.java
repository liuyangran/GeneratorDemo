package com.example.generatordemo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.generatordemo.mapper.UserMapper;
import com.example.generatordemo.model.entity.UserDo;
import org.apache.catalina.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@SpringBootTest
class GeneratorDemoApplicationTests {

    @Autowired(required = false)
    private UserMapper userMapper;

    @Test
    void testInsert(){
        UserDo user = new UserDo();
        user.setName("fish");
        user.setAge(18);
        user.setEmail("fish@salt.com");

        int id = userMapper.insert(user);

        System.out.println(id);
    }

    @Test
    public void testQuery(){
        QueryWrapper<UserDo> userDoQueryWrapper = new QueryWrapper<>();

        userDoQueryWrapper.select("id", "name", "age")
                .ge("age",24);

        userMapper.selectList(userDoQueryWrapper)
                .forEach(System.out::println);
    }

    @Test
    public void testUpdate(){

        UpdateWrapper<UserDo> userDoUpdateWrapper = new UpdateWrapper<>();
        userDoUpdateWrapper.set("name", null);
//        userDoUpdateWrapper.set("email","fishsalt@ss.com");
//        userDoUpdateWrapper.setSql("age = age + 2");
        userDoUpdateWrapper.eq("id",20);

        UserDo userDo = new UserDo();
        userDo.setId(20);
        userDo.setName("哇哈哈");
        userDo.setAge(11);
        userMapper.update(userDo, userDoUpdateWrapper);
//        userMapper.updateById(userDo);

//        UserDo userDo = new UserDo();
//        userDo.setId(16L);
//        userDo.setAge(23);
//        userDo.setName("ywx");
//
//        UserDo userDo1 = new UserDo();
//        userDo1.setId(17L);
//        userDo1.setAge(24);
//        userDo1.setName("LWX");
//
//        CopyOnWriteArrayList<UserDo> userDos = new CopyOnWriteArrayList<>();
//        userDos.add(userDo1);
//        userDos.add(userDo);
//
//        userMapper.updateBatch(userDos);
    }


    @Test
    public void testSelectMaps(){

        QueryWrapper<UserDo> userDoQueryWrapper = new QueryWrapper<>();

        List<Map<String, Object>> mapList = userMapper.selectMaps(userDoQueryWrapper);
        mapList.forEach(map ->{
            System.out.println(map.toString());
        });
        System.out.println(mapList);
    }


}
