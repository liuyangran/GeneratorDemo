package com.example.generatordemo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;


public class Tutorial {

    private static final transient Logger logger = LoggerFactory.getLogger(Tutorial.class);

    public static void main(String[] args) {
        logger.info("第一个shiro程序");

        IniSecurityManagerFactory iniSecurityManagerFactory = new IniSecurityManagerFactory("classpath:shiro.ini");
        SecurityManager securityManager = iniSecurityManagerFactory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        Subject currentUser  = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        session.setAttribute("somkey", "itsvalue");

        if(!currentUser.isAuthenticated()){
            UsernamePasswordToken token = new UsernamePasswordToken("lonestarr", "vespa");

            token.setRememberMe(true);

            try{
                currentUser.login(token);
            } catch (UnknownAccountException e){
                System.out.print("账户不存在:"+e.getMessage());
            }catch (LockedAccountException e){
                System.out.print("账户被锁定:"+e.getMessage());
            }catch (IncorrectCredentialsException e){
                System.out.print("密码不匹配:"+e.getMessage());
            }
        }

        Assert.isTrue(currentUser.isAuthenticated(), "用户已登录");
        logger.info("用户" + currentUser.getPrincipal() + "成功登录");

        if(currentUser.hasRole("schwartz")){
            logger.info("May the Schwartz be with you!" );
        } else {
            logger.info( "Hello, mere mortal." );
        }

        if(currentUser.isPermitted("lightsaber:wield")){
            logger.info("You may use a lightsaber ring.  Use it wisely.");
        } else {
            logger.info("Sorry, lightsaber rings are for schwartz masters only.");
        }

        if ( currentUser.isPermitted( "winnebago:drive:eagle5" ) ) {
            logger.info("You are permitted to 'drive' the 'winnebago' with license plate (id) 'eagle5'.  " +
                    "Here are the keys - have fun!");
        } else {
            logger.info("Sorry, you aren't allowed to drive the 'eagle5' winnebago!");
        }

        currentUser.logout();
        System.exit(0);
    }

}
