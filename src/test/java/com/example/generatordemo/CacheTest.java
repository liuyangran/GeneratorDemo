package com.example.generatordemo;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class CacheTest {

    public static void main(String[] args) {
        // 创建缓存管理器
        CacheManager cacheManager = CacheManager.create("./src/main/resources/ehcache.xml");

        // 获取缓存对象
        Cache cache = cacheManager.getCache("HelloWorldCache");

        // 创建元素
        Element element = new Element("keyone", "valueone");

        // 将元素添加到缓存
        cache.put(element);

        // 获取缓存
        Element keyone = cache.get("keyone");
        System.out.println(keyone);
        System.out.println(keyone.getObjectValue());
        System.out.println(cache.getSize());

        // 删除元素
        cache.remove("keyone");
        System.out.println(cache.get("keyone"));

        // 刷新缓存
        cache.flush();

        // 关闭缓存管理器
        cacheManager.shutdown();


    }
}
