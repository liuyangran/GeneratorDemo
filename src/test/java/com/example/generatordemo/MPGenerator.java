package com.example.generatordemo;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * author:liuyangran
 * describe:mybatis-plus自动生成代码
 */

@SpringBootTest
public class MPGenerator {


    @Value("${spring.datasource.driver-class-name}")
    private String driverName;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    @Test
    public void GeneratorTest(){

        // 创建代码生成器
        AutoGenerator atuoGenerator = new AutoGenerator();
        // 设置模板引擎，默认为velocity
        atuoGenerator.setTemplateEngine(new FreemarkerTemplateEngine());


        // 全局配置
        GlobalConfig gcfg = new GlobalConfig();
        // 此项目路径
        String projectPath = System.getProperty("user.dir");
        // 代码生成于哪个包下
        gcfg.setOutputDir(projectPath + "/src/main/java")
                // 作者名
                .setAuthor("liuyangran")
                // 生成后是否打开资源管理器
                .setOpen(false)
                // 重新生成时文件是否覆盖
                .setFileOverride(true)
                // 开启activeRecord模式(即通过model进行CRUD)
                .setActiveRecord(true)
                // 去掉service接口的首字母I
                .setServiceName("%sService")
                // 设置实体类名称
                .setEntityName("%sDO")
                // 主键策略
                .setIdType(IdType.AUTO)
                // 定义生成的实体类中日期类型
                .setDateType(DateType.ONLY_DATE)
                // 是否开启swagger2模式
                .setSwagger2(false)
                // XML ResultMap: mapper.xml生成查询映射结果
                .setBaseResultMap(true)
                // XML ColumnList: mapper.xml生成查询结果列
                .setBaseColumnList(true);

        atuoGenerator.setGlobalConfig(gcfg);


        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(url)
                .setDriverName(driverName)
                .setUsername(username)
                .setPassword(password)
                .setDbType(DbType.MYSQL);

        atuoGenerator.setDataSource(dsc);


        // 包配置
        PackageConfig pkc = new PackageConfig();
        // 模块名
        pkc.setModuleName(null)
                // 配置父包名
                .setParent("com.example.generatordemo")
                // 配置各包名
                .setController("controller")
                .setService("service")
                .setServiceImpl("service.impl")
                .setMapper("mapper")
                .setEntity("model.entity");

        atuoGenerator.setPackageInfo(pkc);


        // 策略配置
        StrategyConfig strcfg = new StrategyConfig();
         // 是否大写命名
        strcfg.setCapitalMode(true)
                // 数据库表映射到实体的命名策略 (下划线转驼峰)
                .setNaming(NamingStrategy.underline_to_camel)
                // 数据库表字段映射到实体的命名策略
                .setColumnNaming(NamingStrategy.underline_to_camel)
                // 生成实体时去掉表前缀
                .setTablePrefix(pkc.getModuleName() + "_")
                // 开启lombok setter链式编程（@Accessors(chain = true)）
                .setEntityLombokModel(true)
                // restful api 风格控制器
                .setRestControllerStyle(true)
                // url中驼峰转连字符
                .setControllerMappingHyphenStyle(true)
                // 生成实体类字段注解
                .setEntityTableFieldAnnotationEnable(true)
                // 对哪张表生成代码
                .setInclude("user");

        atuoGenerator.setStrategy(strcfg);


        // 开始执行
        atuoGenerator.execute();
    }
}